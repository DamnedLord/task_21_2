#include <iostream>

class IA
{
public:
    IA()
    {
        std::cout << "A constructed" << std::endl;
    }

    ~IA()
    {

    }

    virtual void ShowInfo() = 0;
};

void IA::ShowInfo()
{
    std::cout << "default implementation" << std::endl;
}

class B : virtual public IA
{
public:
    B()
    {
        std::cout << "B constructed" << std::endl;
    }

    ~B()
    {

    }

    virtual void ShowInfo() override
    {
        IA::ShowInfo();
        std::cout << "Show info in class B" << std::endl;
    }
};

class C : virtual public IA
{
public:
    C()
    {
        std::cout << "C constructed" << std::endl;
    }

    ~C()
    {

    }

    virtual void ShowInfo() override
    {
        IA::ShowInfo();
    }
};

class D : virtual public B, virtual public C
{
public:
    D()
    {
        std::cout << "D constructed" << std::endl;
    }

    ~D()
    {

    }

    virtual void ShowInfo() override
    {
        IA::ShowInfo();
    }

};

int main()
{
    D* a = new D;
    delete a;
}